Esta es una guía de como hice mi consola portátil con Raspberry Pi y RetroPie.

---------------

La guía está escrita en _Markdown_ (_md_), y después genero un _HTML_ con
[_Pandoc_](http://pandoc.org/]).

También puede ser útil tener la guía en formato _PDF_ así que también la generé
con Pandoc.

---------------

Para leer la guía en _HTML_: <https://mbernardi.gitlab.io/guia_mi_consola_portatil/>

Para leer la guía en _PDF_: <https://mbernardi.gitlab.io/guia_mi_consola_portatil/articulo.pdf>
