%Mi Consola portátil con Raspberry Pi

Intro
=====

![Consola terminada](fotos/portada_1.jpg)

![Consola vista desde atrás](fotos/portada_2.jpg)

Hice mi consola portátil con Raspberry Pi, a lo mejor a alguien le sirve como
referencia para hacerse una. Escribo esto mientras se pasan 50GB de ROMs y se
scrapean. De paso me queda a mí la sección del Software y la guía de instalación
para cuando tenga que reinstalar algo.

[Ver esto en mi GitLab](https://gitlab.com/mbernardi/guia_mi_consola_portatil).

Hay muchas consolas más en internet para comparar. Esta es bastante distinta a
las otras:

Ventajas
-----

- Quise hacerla lo más fácil de contruir posible, porque es la parte que menos
  me gusta hacer y porque no me animaba a hacer algo complejo en 3D.
  
- Quería tener un joystick completo (como de PlayStation), así puedo jugar a lo
  que sea, hay otras en internet que tienen un joystick modificado, la idea es
  buena, pero perdía botones y además quería algo fácil de hacer.
  
- No quería comprar cosas que al final no sirvan, también tenía miedo de
  quedarme sin ganas y abandonarlo, se puede probar casi todo antes de armarla y
  las partes pueden tener otros usos (el power bank, el joystick y la Raspberry
  Pi).
  
- No tiene cosas difíciles de conseguir, porque acá no es fácil conseguir cosas,
  no puedo pedir nada de Amazon o eBay ni conseguir algún modelo específico de
  pantalla por ejemplo.
  
- Es lo más barato posible o bastante cerca.

- La estructura es simple, se puede reemplazar todo, (por ejemplo el joystick)
  sin cambiar otras cosas.
  
- Quería dejar todos los puertos visibles y disponibles. Tengo el puerto
  ethernet, el HDMI, para auriculares, y 3 usb (uno ocupado por el joystick).
  Puedo conectar otro joystick y jugar de a varias personas.
  
- Se puede jugar de forma portátil, apagar, conectar a una TV, prender y jugar
  perfectamente en HDMI con 1080p sin tener que configurar nada.
  
- Quería que se puedan hacer cosas no convencionales para cuando quiera hacer
  algo raro. Se puede conectar independientemente la energía de la RPi y de la
  pantalla (hubo una vez que usé la consola como router portátil y no necesitaba
  pantalla, además se puede desconectar para ahorrar energía). Se puede
  conectar a un tomacorriente sin usar batería (para usarla conectada al TV),
  tiene un puerto ethernet disponible, etc.
  
- No quería tocar la Raspberry Pi, así la puedo usar para otra cosa. No le soldé
  nada.
  
- Tiene una batería para cargar celulares común y corriente (power bank se
  llaman en algunos lados). Entonces si algo salía mal me quedaba con una
  batería que me venía bien para otras cosas. Además de esa forma me salvé de
  comprar reguladores de tensión o de tener que inventar algo.
  
- Tiene mucha autonomía, 10000mAh.
  
- Puedo cargar el celular, aunque no puedo jugar mientras cargo el celular.
  También puedo cambiar la batería.

- Puedo alimentar la pantalla por USB, eso hace las cosas mucho más fácil.

Desventajas
-----

- Tardé muchísimo en hacerla, creo que más o menos 8 meses. Pasa que tardé mucho
  en comprar las cosas de a una por una, después armé todo con un joystick que
  ya tenía y usé una caja de tomacorriente recortada. Una vez que vi que todo
  funcionaba tardé como 3 semanas en diseñar una caja nueva para imprimir en 3D.
  A las cosas las hacía los fines de semana y además hubieron algunos en los que
  no hice nada. Para armarla se debe tardar unos 2 días. Pero hasta conseguir
  todas las cosas seguro que se tarda más de un mes. Igual no se por que puse
  esto en desventajas, para hacer otras consolas se debe tardar todavía más.
  
- Es grande y pesada, pero no muy fea por lo menos.

- El puerto HDMI sale de la caja por medio de un adaptador en L, el problema es
  que al conectar un cable se le hace mucha fuerza al puerto de la Raspberry Pi.
  Entonces paso una cinta por atrás del adaptador que hay que tirar cuando se
  conecta algo. No es algo muy prolijo, por lo menos la cinta no se ve cuando
  está guardada, más adelante hay fotos.

- No tiene parlantes internos, hay que usar auriculares. Pero con HDMI de todos
  modos hay audio.

Ingredientes
=====

Más abajo explico más cosas sobre cada uno.

- Raspberry Pi 3.

- Tarjeta microSD.
  
- Pantalla de video compuesto.
  
- Joystick, usé un Logitech F310.
  
- Batería para cargar celulares por USB, se llaman power bank en algunos lados.
    
- Carcasa impresa en 3D, lo diseñé yo.

- Conector macho de 3.5mm con 4 contactos.
  
- Conector hembra de 3.5mm con 3 contactos.

- Adaptador HDMI en L.

- Algunos tornillos y cables:
  
    + Dos cables USB tipo A que pueda cortar, uno de los dos tiene que tener la
      otra punta microUSB para la Raspberry Pi.

    + Si se consigue algún conector USB tipo A bien cortito mejor, para romperlo
      y usarlo como conector para el joystick, más abajo se puede ver fotos de
      lo que quise hacer. Sino hay romper el cable del joystick como terminé
      haciendo yo.

    + Algunos cables para conectar las cosas dentro de la consola, para el audio
      y el video.
    
- Abrojo o velcro para sostener la batería.

- Un pedazo de metal que se pueda usar para unir el joystick con la carcasa.

- Teclado bluetooth que tenía dando vueltas, en realidad no hace falta.

- Taladro y soldador de estaño.

Raspberry Pi 3
-----

En realidad al principio usaba una Raspberry Pi 2 pero me compré otra. Vale la
pena la 3 para emular juegos 3D, el WiFi y el Bluetooth.

A propósito hice todo para que no haga falta soldarle nada ni modificarla.

Tarjeta microSD
-----

Por lo menos tiene que ser de 4GB, aunque no compraría de menos de 16GB, depende
de qué ROMs uno quiera pasar. Las ROMs pesadas son las de PSX, N64 y MAME, en
ese orden.

Yo compré una de 64GB porque es lo que necesitaba, para darse una idea descargué
todas las ROMs que pude, lo que pesan son:

- 9GB para NES, SNES, N64, NeoGeo, GBA, Megadrive y otras que no voy a jugar
  nunca.

- 19GB para 35 juegos de PSX.

- 48GB pesa el romset completo de MAME2003, voy a tener que ver como se reduce
  la cantidad de juegos porque no entran. No es cuestión de borrar juegos porque
  unos dependen de otros.

Pantalla de video compuesto
-----

Es lo más barato que hay para usar de pantalla. Lo importante es el tamaño, que
sea de video compuesto y que se pueda alimentar por USB (5V). Lo malo es la baja
calidad, pero no me molesta mucho porque son juegos que se jugaron en pantallas
de mala calidad justamente.

Para encontrarla hay que buscar con las palabras clave 'car backup screen tft
composite 3.5 monitor lcd rearview'.

![Caja con algunos datos técnicos.](fotos/pantalla_3.jpg)

![Pantalla sin los plásticos, en otras fotos se puede ver que tiene una placa
controladora atrás.](fotos/pantalla.jpg)

![Viene con unos botones para controlar brillo, contraste, etc. Se los saqué
pero los guardo por si alguna vez quiero cambiar el brillo.
](fotos/pantalla_2.jpg)

No todas se pueden alimentar por 5V que da un USB, porque son normalmente para
12V y un mismo modelo de pantalla tiene distintas versiones. Entonces es
bastante de suerte. En algunos casos hay que hacerles unas modificaciones, en mi
caso la puedo alimentar simplemente con 5V en vez de 12V y funciona.

En este [thread](https://www.raspberrypi.org/forums/viewtopic.php?f=41&t=17651&sid=2a7891f9f885352d92cd8dd53a6bbf2b)
hay mucha información sobre las distintas versiones.

Hay muchas alternativas, y la Raspberry Pi está llena de puertos para pantallas,
todas son alternativas muy buenas:

- Por HDMI, las cosas a ver es que son caras, grandes, consumen mucha energía,
  y no se si algo más.
  
- Por los pines GPIO, por lo que vi son muy chicas y cuidado porque leí que no
  llegan a muchos fps y pueden ser lentas para responder.
  
- Por el conector que dice DISPLAY, por lo que vi hay muy pocas que usan ese
  conector y creo que son todas oficiales de Raspberry.
  
- No se si habrá alguna interfaz estándar para pantallitas TFT que se pueda
  usar, creo que es SPI, pero no se como funciona ni si es compatible, vale la
  pena fijarse porque puede ser la mejor alternativa.

Joystick
-----

Yo quería uno con muchos botones para poder jugar al sistema que sea, con
los botones que necesita una Playstation esta bien, no hay sistemas que usen más
que eso (creo que los únicos serían algunos juegos de Sega Genesis, pero se
pueden usan los botones L y R para los que faltan. Ver imágenes de
[esta página.](https://github.com/RetroPie/RetroPie-Setup/wiki/Genesis-Megadrive)).

Lo más importante es que tenga pad direccional (D-Pad) de buena calidad. Es muy
importante el D-Pad porque son los botones que uno usa para moverse, son los
botones más importantes, usar un analógico no es lo mismo, si o si hay que tener
un buen D-Pad.

- Xbox 360: Tiene pad direccional muy malo y drivers no muy libres, pero al
  final los drivers andan. No usaría uno inalámbrico porque no quiero tener que
  cargarlo.
  
- Playstation: No se si se pueden usar de PS1 y PS2. De PS3 y PS4 se pueden usar
  pero son inalámbricos. El D-Pad es muy bueno.
  
- Para PC: Los genéricos generalmente tienen un D-Pad muy malo. El mejor que
  encontré es el Logitech F310 que tiene un D-Pad bastante bueno y es de buena
  calidad, es el que terminé comprando.
  
- De alguna consola: Se pueden usar Joysticks que no son USB, creo que se
  conectan a los GPIO y se debe usar un driver especial, pero vale la pena ver.
  No usé uno de esos porque los únicos que tienen muchos botones son los de N64
  y Gamecube. El de N64 no tiene botones X e Y (no sé si se podrán usar los
  C-Buttons como reemplazo) y el de Gamecube tiene un D-Pad muy malo.
  Si no es importante la cantidad de botones, es una muy buena idea porque
  generalmente tienen los mejores D-Pads y además quedan mejor estéticamente.
  
Al final compré el Logitech F310. Es de buena calidad y el D-Pad no es tan malo,
está bastante bien. Además tiene muy buenos drivers, es más, se puede poner en
modo DInput o XInput, al final uso DInput porque no me acuerdo que me andaba mal
con XInput. Pero si pueden usar XInput es mejor.

![Placa del joystick, se ve de buena calidad. En otras fotos se puede ver desde
afuera.](fotos/joystick_1.jpg)

![Placa vista de atrás, los Triggers analógicos también se ven muy buenos.
](fotos/joystick_2.jpg)

Batería
-----

Uso un power bank para celulares porque es lo más fácil y además se puede usar
para cargar el celular.

Compré una batería TP-Link TL-PB10400 de 10000mAh porque tiene dos puertos (uno
de 2A para la RPi y otro de 1A para la pantalla), mucha capacidad y una forma
que queda bastante bien abajo del joystick.

![La batería que uso.](fotos/bateria.jpg)

Carcasa
-----

Creo que la única forma es imprimirla en 3D, pero mientras tanto para probar,
primero la hice recortando una caja de tomacorriente.
  
![Caja de tomacorriente que compré.](fotos/caja_cambre.jpg)

![Tuve que recortar la tapa para la pantalla y hacerle un agujero atrás para los
cables. Para unir la tapa y la base tuve que usar cinta (Los tornillos quedaron
tapados por la pantalla y de paso los usé para regular la altura).
](fotos/viejo_1.jpg)

![Agujeros para los puertos USB y ethernet.](fotos/viejo_2.jpg)

A la caja la hice con FreeCAD, pero no lo recomiendo mucho al programa. Mientras
que exporte al formato STL está bien, es el formato más común para impresiones.
Me lo imprimió una persona que se dedica a eso, yo le pasé el STL y listo.

Creo que conviene primero comprar y medir todo, hasta los tornillos. Y antes
dibujar todo en papel junto con las medidas. Así cuando se hace el modelo ya
está todo definido y no hay que inventar nada, porque me pasó que en FreeCAD se
me hace bastante difícil cambiar medidas (se puede desacomodar todo).

No hay que olvidarse de nada. Son dos tapas unidas por tornillos que pasan de
lado a lado, hay varias formas de atornillar partes impresas en 3D pero no me
animé a hacerlo de otra forma. Tiene agujeros para los puertos USB, Ethernet,
HDMI y de auricular. Para sostener el jack de auricular le puse unos bloques así
queda bien firme.

También hay que ver cómo poner la pantalla en el lugar, para apretarla contra la
tapa uso tornillos, y para centrarla uso unas tiras de plastico que están en la
tapa. Por último hay que hacerle agujeros para los tornillos y para los cables.

A la Raspberry Pi la puse abajo a la izquierda, para sacar directamente los
puertos USB y para que pueda conectar la energía y el jack de 3.5mm sin tener
que soldar nada. Como desventaja para usar el HDMI tuve que usar un adaptador
en L, y el cable del joystick quedó visible. Pero igual quedó bastante bien.

Es la primera vez que imprimo algo en 3D, le dejé 1mm de margen a todas las
cosas y tuve la suerte que al final todo encajó muy bien. Tuve que cortar
algunas cosas con un cutter y agrandar algunos agujeros. Ahí me di cuenta que
el plástico PLA es **muy** duro.

![La base, está al revés, arriba a la izquierda está para el puerto de
auriculares.](fotos/caja_1.jpg)

![La tapa.](fotos/caja_2.jpg)

![Como queda cerrada.](fotos/caja_3.jpg)

![El agujero para los auriculares.](fotos/caja_4.jpg)

![Los agujeros para los puertos.](fotos/caja_5.jpg)

![De paso hay que aprovechar a hacerse un llavero.](fotos/llavero.jpg)

Links a los archivos:

[base.fcstd](archivos/base.fcstd)\
[base.stl](archivos/base.stl)\
[tapa.fcstd](archivos/tapa.fcstd)\
[tapa.stl](archivos/tapa.stl)

[llavero_debian.fcstd](archivos/llavero_debian.fcstd)\
[llavero_debian.stl](archivos/llavero_debian.stl)

Conectores de 3.5mm
-----

Se necesita uno macho con 4 contactos, porque la RPi saca dos canales de audio,
el video y la tierra. Es como los que tienen los auriculares que traen
micrófono.

También compré uno **hembra** estéreo, como lo uso para auriculares no lleva
video y tiene 3 contactos.

![Cómo son los conectores en la Raspberry Pi a la izquierda, y a la derecha
un conector de 3 contantos para auriculares.](imagenes/conectores_3.5mm.png)

![Acá se pueden ver los conectores, el amarillo es de video.
](fotos/conexiones_1.jpg)

Adaptador HDMI en L
-----

Porque es la mejor forma que encontré de sacar el HDMI afuera de la carcasa,
hay que fijarse bien que el adaptador doble para el lado correcto, yo quería que
doble hacia abajo.

![Se puede ver que tuve que cortar un poco el plástico para que entre en el
agujero de la caja.](fotos/hdmi.jpg)

Hardware
=====

Primero fui comprando las cosas y las fui probando:

- Que la Raspberry Pi corra bien a RetroPie en HDMI.
- Que el joystick funcione.
- Que la pantalla de video compuesto funcione con la Raspberry Pi.
- Que el audio y el video funcionen al mismo tiempo.
- Que se pueda alimentar a la pantalla con 5V.
- Que la Raspberry Pi, la pantalla y el joystick funcionen con el power bank
  (al ser USB es de 5V).

A esas pruebas las fui haciendo sin hacer los cables, por las dudas que al final
algo no funcione y termine haciendo otra cosa. El único cable que tuve que hacer
es el de audio y video.

![Cable que hay que fabricar para el audio y el video.](fotos/conexiones_1.jpg)

Una vez que ví que todo funcionaba, armé todo haciendo la estructura así nomás.
La batería estaba agarrada con cinta, y para la caja recorté una caja de
tomacorriente porque tenía un tamaño bastante bueno. Adentro dejé los cables
bien largos (por si corto uno demasiado corto y después necesito uno más largo)
y también las cosas estaban sueltas.

![Cables para audio, video, y alimentación de pantalla.](fotos/conexiones_2.jpg)

![Como quedó la consola vieja.](fotos/viejo_3.jpg)

![De atrás.](fotos/viejo_4.jpg)

Después diseñé la caja para imprimirla en 3D y armé todo de nuevo.

El joystick va agarrado de la caja con un pedazo de metal unido por tornillos,
y la batería va colgada del pedazo de metal con un velcro que le da la vuelta.

Entre la batería y el joystick puse
[goma eva](https://es.wikipedia.org/wiki/Etilvinilacetato) para que la batería
no se apoye en la cabeza de los tornillos que unen el metal con el joystick,
tambien ayuda a que la batería no resbale tanto.

![Metal que une el joystick con la caja, tuve que hacer un agujero de más (el
más grande) para poder atornillar por ahí un tornillo del joystick.
](fotos/estructura_0.jpg)

![Como queda después de unir, los tornillos del joystick están al revés, deben
ir con la cabeza afuera y las tuercas adentro.](fotos/estructura_1.jpg)

![De costado.](fotos/estructura_2.jpg)

![El velcro para sostener la batería.](fotos/estructura_3_a.jpg)

![El velcro pasa entre el metal y el joystick.](fotos/estructura_3_b.jpg)

![Foto de dónde quedaron los tornillos del joystick.](fotos/estructura_4_a.jpg)

![Agujeros del joystick vistos desde fuera.](fotos/estructura_4_b.jpg)

![Después de atornillar al joystick. Se puede ver la goma eva que puse.
](fotos/estructura_5.jpg)

![Tornillos en la caja, los tapé con cinta por las dudas que hagan contacto
con la Raspberry Pi.](fotos/estructura_6.jpg)

![Como quedó todo armado.](fotos/estructura_7.jpg)

Los cables que van de la caja a la batería van por fuera, el cable del
joystick va por dentro de la caja y va conectado a los puertos USB.

![Diagrama de las conexiones que hay que hacer.
](imagenes/diagrama_conexiones.png)

![Foto de las conexiones dentro de la caja, sin incluir la del joystick.
](fotos/conexiones_3.jpg)

![Con la Raspberry Pi.](fotos/conexiones_4.jpg)

![Para el joystick rompí un conector USB tipo A, buscando la forma de hacerlo
lo más corto posible. Antes rompí un pendrive USB bien cortito, pero no me
sirvió entonces rompí este conector.](fotos/conexiones_5.jpg)

![Al final no pude acortar el plástico blanco, lo que hice fue acortar el metal
y envolver con cinta.](fotos/conexiones_6.jpg)

![Foto de como queda conectado, los cables pasan al lado del puerto.
](fotos/conexiones_7.jpg)

![Como quedaron todos los cables dentro de la caja.](fotos/conexiones_8.jpg)

![Foto de como quedaron los puertos USB (falta el cable del joystick).
](fotos/terminado_1.jpg)

![El puerto para los auriculares quedó muy bien.](fotos/terminado_2.jpg)

![Salida HDMI que quedó abajo.](fotos/terminado_3.jpg)

![Cuando uno conecta algo al HDMI se le hace mucha fuerza, le puse una cinta
alrededor del adaptador HDMI que hay que tirar mientras se conecta un cable. De
esa forma no se le hace fuerza a la Raspberry Pi. Es lo único que se me ocurrió,
no quiero pegar el adaptador a la caja. La cinta se puede empujar hacia dentro
de la caja, entonces no queda colgando.](fotos/terminado_4.jpg)

![En esta foto anterior se puede ver como queda la cinta cuando está guardada.
](fotos/estructura_7.jpg)

![Foto de como queda la conexión al joystick vista desde fuera.
](fotos/terminado_5.jpg)

![Otra foto de todo terminado.](fotos/terminado_6.jpg)


Software
=====

Uso [RetroPie](https://retropie.org.uk/), es una distribución Linux creada
especialmente para jugar a juegos retro en la Raspberry Pi. Está muy bien hecha,
para la emulación usa principalmente RetroArch, para la interfaz
EmulationStation, y está basada en Raspbian.

Se puede manejar completamente con Joystick gracias a EmulationStation y a
algunos scripts propios de RetroPie. Se pueden configurar joysticks, conectarse
a dispositivos WiFi y Bluetooth, y configurar algunas cosas todo usando el
Joystick.

Para instalarlo se debe descargar la imagen ISO, y grabar en una tarjeta microSD
como con cualquier distribución para Raspberry Pi. Para grabar en Linux hay que
usar `dd`, y para Windows se puede usar Rufus.

Para pasar las ROMs se puede conectar la microSD a la PC, o se puede usar un
pendrive.

Para eso hay que entrar a
[la wiki de RetroPie](https://github.com/RetroPie/RetroPie-Setup/wiki), es muy
completa.

ROMs
-----

Son los juegos, es ilegal descargar ROMs entonces ningún sitio oficial te dice
donde encontrarlos.

BIOS
-----

Algunos emuladores además de las ROMs necesitan BIOS que van en
`~/RetroPie/BIOS` a excepción de NeoGeo que debe tener la BIOS en la carpeta de
ROMs.

[Acá hay más información](https://github.com/RetroPie/RetroPie-Setup/wiki)

Las BIOS tampoco se pueden conseguir de forma legal.

Otros
-----

Algunas cosas más sobre RetroPie que son bastante importantes, también vale la
pena leerse la [wiki](https://github.com/RetroPie/RetroPie-Setup/wiki) entera
para ver qué se puede hacer.

### Editando `/boot/config.txt`

Es un archivo de configuración para algo así como la BIOS y sistema operativo,
lleva algunas opciones sobre el audio y el video generalmente, si uno quiere
overclockear la placa también se debe modificar este archivo.

Como uso una pantalla de video compuesto, tengo que acomodar un poco la imagen,
las opciones que tuve que agregar yo al archivo son:

~~~~~
sdtv_mode=2
sdtv_aspect=3

disable_overscan=0
overscan_right=-31
overscan_left=-8
overscan_top=-15
overscan_bottom=-15
framebuffer_width=480
framebuffer_height=288
disable_audio_dither=1
~~~~~

Son todas sobre video salvo la última. La última sirve para disminuir el ruido
que se escucha de fondo, pero como desventaja uno debe usar el volumen de
EmulationStation al máximo o se pierde calidad. Entonces se está obligado a usar
auriculares con control de volumen analógico.

Acá hay información sobre las
[opciones disponibles](http://elinux.org/RPiconfig). Acá
[también](https://www.raspberrypi.org/documentation/configuration/config-txt.md).

### Para cambiar tamaño de fuente de terminal

~~~~~
sudo dpkg-reconfigure console-setup
~~~~~

### Si tarda mucho el inicio, esperando red

Desactivar _wait for network at boot_ en `raspi_config`.

### Para usar pendrives

Se montan automáticamente en `/media/usb`.

### Para obtener una terminal

Para llegar a la consola hay que cerrar EmulationStation. Se puede hacer con F4
o abriendo el menú (apretando Start) y cerrando EmulationStation.

Para volver a abrir la interfaz hay que usar el comando `emulationstation`.

### Splash

Se puede cambiar la imagen que aparece al iniciar (la que dice EmulationStation
no se puede cambiar, solamente la que dice RetroPie). Se debe guardar la imagen
en `~/RetroPie/splashscreens/` y activarla en las opciones de RetroPie.

### Temas de EmulationStation

Se instalan en la configuración de RetroPie y se activan en el menú de
EmulationStation (apretando Start). También en el mismo menú me gusta cambiar
la transición a _Slide_ en vez de _Fade_.

### Scrapes

Es obtener la información sobre cada ROM, una imagen del juego, una
descripción, etc. Hay varias formas de hacer scrapes, la que usé es la que
viene con EmulationStation.

No se bien como funciona, tampoco quiero scrapear todas las consolas porque
estoy lleno de ROMs y dice por ahí que te empieza a poner las cosas más lentas.

Guía de instalación
-----

Acá escribo los pasos que seguí para instalar todo, así me queda para cuando lo
necesite.

- Descargar imagen retropie
- Pasar a microSD con dd
- Conectar a raspberry por primera vez
- Configurar WiFi
- Configurar teclado bluetooth
    + Prender teclado
    + Apretar boton de emparejado por unos segundos en el teclado
    + Register and connect to Bluetooth device
    + Aparece en la pantalla
    + Elegir DisplayYesNo
    + Escribir PIN en teclado y apretar Enter
- Instalar tema `simple` desde la configuracion de RetroArch (es el tema que me
  gusta)
    + Poner en el menu de EmulationStation (con boton start)
    + De paso poner transición 'slide'
- Editar /boot/config.txt
    + Salir de EmulationStation desde el menú del botón Start
    + `sudo vi /boot/config.txt` y agregar al final:
  
        ~~~~~
        sdtv_mode=2
        sdtv_aspect=3

        disable_overscan=0
        overscan_right=-31
        overscan_left=-8
        overscan_top=-15
        overscan_bottom=-15
        framebuffer_width=480
        framebuffer_height=288
        disable_audio_dither=1
        overscan_scale=1
        ~~~~~

    + Usar comando `emulationstation` para volver
    + Reiniciar
- Sacar microSD y pasar:
    + ROMs (en `~/RetroPie/ROMS`)
    + Saves (junto con las ROMs)
    + Splash (en `~/RetroPie/splashscreens/splash.png`)
    + BIOS (en `~/RetroPie/BIOS`)
        * Dreamcast: `dc_boot.bin` `dc_flash.bin`
        * GBA: `gba_bios.bin`
        * Famicom (NES): `disksys.rom`
        * PC Engine: `syscard3.pce`
        * PS1: `SCPH1001.BIN`
        * Saturn: `saturn_bios.bin`
    + Pasar BIOS de NeoGeo a `~/RetroPie/roms/neogeo`
        * `neogeo.zip`
- Cambiar splash en configuración de RetroPie

Backup
-----

Si algún día llego a formatear tengo que hacer backup de:

- Saves (En cada carpeta de ROMs)
  Tienen extensión .srm, salvo en n64 que pueden ser .eep, .mpk
  Los savestates tienen una extensión que no se cual es.

- BIOS (No hace falta backup pero sí ver si no agregué uno que no está en la
  guía).

- Splash.

- `/boot/config.txt` (No copiar y pegar, sino escribir encima del nuevo config
  las dudas que un nuevo RetroPie venga con nuevas opciones).

- Scrapes.
