# Makefile
#
# Convierte todo lo que se llama *.md
#
# Para crear html:
#   make  
#
# Para crear html y css:
#   make all
#
# Para borrar archivos generados:
#   make clean

SOURCE_DOCS := $(wildcard *.md)

EXPORTED_DOCS=\
 $(SOURCE_DOCS:.md=.html) \
 $(SOURCE_DOCS:.md=.pdf)

.DEFAULT_GOAL := $(SOURCE_DOCS:.md=.html)

RM=/bin/rm

PANDOC=pandoc

PANDOC_OPTIONS= -c articulo.css --toc --toc-depth 3

PANDOC_HTML_OPTIONS=--to html5
PANDOC_PDF_OPTIONS=

# Pattern-matching Rules

%.html : %.md
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_HTML_OPTIONS) -o $@ $<

%.pdf : %.md
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_PDF_OPTIONS) -o $@ $<


# Targets and dependencies

.PHONY: all clean

all : $(EXPORTED_DOCS)

clean:
	- $(RM) $(EXPORTED_DOCS)
